package ru.sidm.yotatesttask;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import ru.sidm.yotatesttask.engine.Model;
import ru.sidm.yotatesttask.engine.WorkerFragment;

public class MainActivity extends AppCompatActivity implements Model.Observer {

    private static final String TAG_WORKER_FRAGMENT = "loading_worker_fragment";

    private Model model;

    EditText etURL;
    ImageView ivUpdate;
    TextView tvHTMLText;
    ProgressBar pbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        WorkerFragment workerFragment = (WorkerFragment) getFragmentManager().findFragmentByTag(TAG_WORKER_FRAGMENT);
        if(workerFragment == null){
            workerFragment = new WorkerFragment();
            getFragmentManager().beginTransaction().add(workerFragment,TAG_WORKER_FRAGMENT).commit();
        }

        model = workerFragment.getModel();


        etURL = (EditText)findViewById(R.id.urllink);
        ivUpdate = (ImageView)findViewById(R.id.update);
        tvHTMLText = (TextView)findViewById(R.id.urlhtml);
        pbLoading = (ProgressBar)findViewById(R.id.progress);

        if(savedInstanceState == null){
            etURL.setText(getResources().getString(R.string.yota_link));
        }else {
            etURL.setText(model.getUrl());
            tvHTMLText.setText(model.getHtmlText());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        model.registerObserver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        model.unregiserObserver(this);
    }

    private void setControlEnable(boolean enableValue){
        etURL.setEnabled(enableValue);
        ivUpdate.setEnabled(enableValue);
        if(enableValue)
            pbLoading.setVisibility(View.GONE);
        else
            pbLoading.setVisibility(View.VISIBLE);
    }

    public void onLoadClick(View view){
        model.loadURL(etURL.getText().toString());
    }


    @Override
    public void onURLLoadingStarted() {
       setControlEnable(false);
    }

    @Override
    public void onURLLoadingSucceeded() {
        tvHTMLText.setText(model.getHtmlText());
        setControlEnable(true);
    }

    @Override
    public void onURLLoadingFailed(String errorDescription) {
        setControlEnable(true);
        String errorMessage = getResources().getString(R.string.loading_error_message) + "\n" + errorDescription;
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }
}
