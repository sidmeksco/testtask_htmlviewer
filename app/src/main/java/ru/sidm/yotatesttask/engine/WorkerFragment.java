package ru.sidm.yotatesttask.engine;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by sidm on 16.02.2016.
 */
public class WorkerFragment extends Fragment {
    private Model model;

    public WorkerFragment(){
        this.model = new Model();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public Model getModel(){
        return this.model;
    }

}
