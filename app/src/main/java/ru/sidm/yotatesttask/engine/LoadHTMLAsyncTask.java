package ru.sidm.yotatesttask.engine;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by sidm on 16.02.2016.
 */
public class LoadHTMLAsyncTask  extends AsyncTask<Void,Void,String> {

    private Model model;
    private String errorDescription;

    public LoadHTMLAsyncTask(Model model){
        this.model = model;
    }

    @Override
    protected String doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;
        InputStream is = null;
        String result = null;

        try {
            URL url = new URL(checkAndCorrectLink(model.getUrl()));
            urlConnection = (HttpURLConnection) url.openConnection();
            int responseCode = urlConnection.getResponseCode();

            if(responseCode == 200){
                StringBuilder resultHTML = new StringBuilder();
                is = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line=null;
                while((line = reader.readLine()) != null){
                    resultHTML.append(line);
                }
                result = resultHTML.toString();
            }else{
                errorDescription = "Ошибка установки соединения. Код: " + responseCode;
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            errorDescription = e.toString();
        } catch (IOException e) {
            e.printStackTrace();
            errorDescription = e.toString();
        }finally {
            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(is != null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s == null){
            model.onURLLoadingFailed(errorDescription);
        }else{
            model.onURLLoadingSucceeded(s);
        }
    }

    private String checkAndCorrectLink(String link){
        if(!link.startsWith("http://") && !link.startsWith("https://"))
            return "http://" + link;
        return link;
    }
}
