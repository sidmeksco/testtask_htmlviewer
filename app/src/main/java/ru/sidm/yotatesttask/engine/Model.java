package ru.sidm.yotatesttask.engine;

import android.database.Observable;

/**
 * Created by sidm on 16.02.2016.
 */
public class Model {

    private String url;
    private String htmlText;
    private LoadURLObservables observables;

    private boolean isWorking;


    public Model(){
        this.observables = new LoadURLObservables();
    }


    public void loadURL(String url){
        if(isWorking){
            return;
        }

        this.url = url;
        isWorking = true;
        observables.notifyURLLoadingStarted();
        LoadHTMLAsyncTask asyncTask = new LoadHTMLAsyncTask(this);
        asyncTask.execute();
    }

    public void onURLLoadingSucceeded(String html){
        this.htmlText = html;
        observables.notifyURLLoadingSucceeded();
        isWorking = false;
    }

    public void onURLLoadingFailed(String errorDescription){
        observables.notifyURLLoadingFailed(errorDescription);
        isWorking = false;
    }


    public String getHtmlText() {
        return htmlText;
    }

    public String getUrl() {
        return url;
    }

    public void registerObserver(Observer observer){
        this.observables.registerObserver(observer);
        if(isWorking){
            observer.onURLLoadingStarted();
        }
    }

    public void unregiserObserver(Observer observer){
        this.observables.unregisterObserver(observer);
    }

    public void unregisterAllObservers(){
        this.observables.unregisterAll();
    }

    public interface Observer{
        void onURLLoadingStarted();
        void onURLLoadingSucceeded();
        void onURLLoadingFailed(String errorDescription);
    }

    private class LoadURLObservables extends Observable<Observer>{
        public void notifyURLLoadingStarted(){
            for(Observer observer : mObservers){
                observer.onURLLoadingStarted();
            }
        }

        public void notifyURLLoadingSucceeded(){
            for(Observer observer : mObservers){
                observer.onURLLoadingSucceeded();
            }
        }

        public void notifyURLLoadingFailed(String error){
            for(Observer observer: mObservers){
                observer.onURLLoadingFailed(error);
            }
        }
    }
}
